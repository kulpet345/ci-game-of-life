from gameoflife import *


def test_all():
    test_init_from_char()
    test_Fish_update_cell()
    test_CrayFish_update_cell()
    test_Rock_update_cell()
    test_Free_update_cell()
    test_sample()
    test_Fish_1()
    test_Fish_2()
    test_CrayFish_1()
    test_CrayFish_2()
    test_Rock()
    test_Composite_Test_1()
    test_Composite_Test_2()
    print('Accepted')


def test_init_from_char():
    obj = Cell.init_from_char('#')
    assert isinstance(obj, Rock)
    obj = Cell.init_from_char('.')
    assert isinstance(obj, Free)
    obj = Cell.init_from_char('F')
    assert isinstance(obj, Fish)
    obj = Cell.init_from_char('C')
    assert isinstance(obj, CrayFish)


def test_Fish_update_cell():
    obj = Fish()
    neighbours = [Fish, Rock, CrayFish, Free]
    new_cell = obj.update_cell(neighbours)
    assert isinstance(new_cell, Free)
    neighbours = [Fish, Fish, Fish, Free]
    new_cell = obj.update_cell(neighbours)
    assert isinstance(new_cell, Fish)


def test_CrayFish_update_cell():
    obj = CrayFish()
    neighbours = [CrayFish, Rock, Fish, Free]
    new_cell = obj.update_cell(neighbours)
    assert isinstance(new_cell, Free)
    neighbours = [CrayFish, CrayFish, CrayFish, Free]
    new_cell = obj.update_cell(neighbours)
    assert isinstance(new_cell, CrayFish)


def test_Rock_update_cell():
    obj = Rock()
    neighbours = [Fish, Rock, CrayFish, Free]
    new_cell = obj.update_cell(neighbours)
    assert isinstance(new_cell, Rock)
    neighbours = [Fish, Fish, Fish, Free]
    new_cell = obj.update_cell(neighbours)
    assert isinstance(new_cell, Rock)


def test_Free_update_cell():
    obj = Free()
    neighbours = [Fish, Fish, Fish, Free]
    new_cell = obj.update_cell(neighbours)
    assert isinstance(new_cell, Fish)
    neighbours = [CrayFish, CrayFish, CrayFish, Free]
    new_cell = obj.update_cell(neighbours)
    assert isinstance(new_cell, CrayFish)
    neighbours = [CrayFish, CrayFish, Free, Free]
    new_cell = obj.update_cell(neighbours)
    assert isinstance(new_cell, Free)


def test_sample():
    op = 1
    n = 3
    m = 5
    field = [['F', '#', '#', '.', '.'], ['.', 'F', 'F', '.', '#'], ['F', 'C', 'C', 'C', '.']]
    fin_field = [['.', '#', '#', '.', '.'], ['F', 'F', '.', '.', '#'], ['.', '.', 'C', '.', '.']]
    res = start_game(op, n, m, field)
    assert res == fin_field


def test_Fish_1():
    op = 1
    n = 3
    m = 3
    field = [['F', 'F', 'F'], ['F', 'F', 'F'], ['F', 'F', 'F']]
    fin_field = [['F', '.', 'F'], ['.', '.', '.'], ['F', '.', 'F']]
    res = start_game(op, n, m, field)
    assert res == fin_field


def test_Fish_2():
    op = 2
    n = 3
    m = 3
    field = [['F', 'F', 'F'], ['F', 'F', 'F'], ['F', 'F', 'F']]
    fin_field = [['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.']]
    res = start_game(op, n, m, field)
    assert res == fin_field


def test_CrayFish_1():
    op = 1
    n = 3
    m = 3
    field = [['C', 'C', 'C'], ['C', 'C', 'C'], ['C', 'C', 'C']]
    fin_field = [['C', '.', 'C'], ['.', '.', '.'], ['C', '.', 'C']]
    res = start_game(op, n, m, field)
    assert res == fin_field


def test_CrayFish_2():
    op = 2
    n = 3
    m = 3
    field = [['C', 'C', 'C'], ['C', 'C', 'C'], ['C', 'C', 'C']]
    fin_field = [['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.']]
    res = start_game(op, n, m, field)
    assert res == fin_field


def test_Rock():
    op = 1
    n = 3
    m = 3
    field = [['#', '#', '#'], ['#', '#', '#'], ['#', '#', '#']]
    fin_field = [['#', '#', '#'], ['#', '#', '#'], ['#', '#', '#']]
    res = start_game(op, n, m, field)
    assert res == fin_field


def test_Free_1():
    op = 1
    n = 3
    m = 3
    field = [['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.']]
    fin_field = [['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.']]
    res = start_game(op, n, m, field)
    assert res == fin_field


def test_Composite_Test_1():
    op = 10
    n = 5
    m = 5
    field = [['.', '#', 'F', 'C', '.'], ['.', 'F', 'F', 'F', '.'], ['.', 'C', 'F', '#', '.'],
             ['#', '.', 'C', '.', '#'], ['.', 'C', 'C', 'C', '.']]
    fin_field = [['.', '#', 'F', '.', '.'], ['.', 'F', '.', 'F', '.'], ['.', '.', 'F', '#', '.'],
             ['#', '.', 'C', 'C', '#'], ['.', '.', 'C', 'C', '.']]
    res = start_game(op, n, m, field)
    assert res == fin_field


def test_Composite_Test_2():
    op = 1
    n = 5
    m = 5
    field = [['F', 'F', 'C', 'F', 'F'], ['F', 'C', 'C', 'C', 'F'], ['F', 'C', '.', 'C', 'F'],
             ['F', 'C', 'C', 'C', 'F'], ['F', 'F', 'C', 'F', 'F']]
    fin_field = [['F', 'F', 'C', 'F', 'F'], ['F', 'C', '.', 'C', 'F'], ['F', '.', '.', '.', 'F'],
             ['F', 'C', '.', 'C', 'F'], ['F', 'F', 'C', 'F', 'F']]
    res = start_game(op, n, m, field)
    assert res == fin_field

