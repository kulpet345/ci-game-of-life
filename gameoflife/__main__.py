from . import lib
import argparse
import sys

from . import utils


def run_programme(input_name, output_name):
    if input_name == sys.stdin:
        cnt_operations, n, m, start_field = utils.input_from_stdin()
    else:
        cnt_operations, n, m, start_field = utils.input_from_file(input_name)
    result = lib.start_game(cnt_operations, n, m, start_field)
    if output_name == sys.stdout:
        utils.output_in_stdout(result)
    else:
        utils.output_in_file(output_name, result)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--read", nargs='?', default=sys.stdin, help=
                        """reads data from input_file or stdin by default""")
    parser.add_argument("-w", "--write", nargs='?', default=sys.stdout, help=
                        """outputs data in output_file or stdout by default""")
    args = parser.parse_args()
    run_programme(args.read, args.write)
