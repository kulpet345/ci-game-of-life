from enum import Enum
import copy


class GameEssences(Enum):
    """ The main game essences and its chars on field"""
    Fish = 'F'
    CrayFish = 'C'
    Free = '.'
    Rock = '#'


class Cell:
    """Basic behaviour cells on field"""
    def get_type(self):
        """Returns game essence of cell"""
        return self.type

    @staticmethod
    def init_from_char(symbol):
        """Initialization from char on field"""
        if symbol == 'F':
            return Fish()
        elif symbol == 'C':
            return CrayFish()
        elif symbol == '#':
            return Rock()
        else:
            return Free()

    @classmethod
    def update_cell(cls, neighbours):
        """Calculate new Cell uses information about neighbours cells"""
        count = neighbours.count(cls)
        if count >= 4:
            return Free()
        elif count < 2:
            return Free()
        else:
            return cls()


class Fish(Cell):
    def __init__(self):
        self.type = GameEssences.Fish


class CrayFish(Cell):
    def __init__(self):
        self.type = GameEssences.CrayFish


class Rock(Cell):
    def __init__(self):
        self.type = GameEssences.Rock

    @classmethod
    def update_cell(cls, neighbours):
        """Redefine basic update_cell"""
        return Rock()


class Free(Cell):
    def __init__(self):
        self.type = GameEssences.Free

    @classmethod
    def update_cell(cls, neighbours):
        """Redefine basic update_cell"""
        count_of_fish = neighbours.count(Fish)
        count_of_cray_fish = neighbours.count(CrayFish)
        if count_of_fish == 3:
            return Fish()
        elif count_of_cray_fish == 3:
            return CrayFish()
        else:
            return Free()


def get_symbol(type_cell):
    """Match Cell with own symbol"""
    if isinstance(type_cell, Free):
        return '.'
    if isinstance(type_cell, Rock):
        return '#'
    if isinstance(type_cell, Fish):
        return 'F'
    if isinstance(type_cell, CrayFish):
        return 'C'


class Ocean:
    """Include field with its length and width"""
    def __init__(self, n, m, list_of_symbols):
        """Initialization from length, width and symbols constituent field"""
        self.n = n
        self.m = m
        self.field = [['.'] * m for i in range(n)]
        for i in range(n):
            for j in range(m):
                self.field[i][j] = Cell.init_from_char(list_of_symbols[i][j])

    def correct_cell(self, posx, posy):
        """Check correct cell by the place"""
        return 0 <= posx < self.n and 0 <= posy < self.m

    def get_neighbours(self, posx, posy):
        """Find neighbours by side or diagonally"""
        neighbours = []
        for i in range(posx - 1, posx + 2):
            for j in range(posy - 1, posy + 2):
                if (i, j) != (posx, posy) and self.correct_cell(i, j):
                    neighbours.append(type(self.field[i][j]))
        return neighbours

    def _update_field(self):
        """Find new field after 1 step of game"""
        new_ocean = Ocean(self.n, self.m, self.field)
        for i in range(self.n):
            for j in range(self.m):
                new_ocean.field[i][j] = self._update_cell(i, j, self.field[i][j])
        return new_ocean

    def _update_cell(self, posx, posy, type_of_cell):
        """Find new cell after 1 step game"""
        neighbours = self.get_neighbours(posx, posy)
        return self.field[posx][posy].update_cell(neighbours)

    def run(self, n):
        """Find new field after n steps of game"""
        for i in range(n):
            self = self._update_field()
        return self

    def get_result(self):
        """Returns copy finished field"""
        result = [['.'] * self.m for i in range(self.n)]
        for i in range(self.n):
            for j in range(self.m):
                self.field[i][j] = get_symbol(self.field[i][j])
        return copy.deepcopy(self.field)


def start_game(cnt_operations, n, m, field):
    """Start game with cnt_operation iterations and current field"""
    game = Ocean(n, m, field)
    game = game.run(cnt_operations)
    res = game.get_result()
    return res
